using Vina.DTO;
using System;
using System.Collections.Generic;

namespace Vina.DAO
{
    public class SqlKeywordDao : SqlDaoBase<Keyword>
    {
        public SqlKeywordDao()
        {
            TableName = "tblKeyword";
            EntityIDName = "KeywordID";
            StoreProcedurePrefix = "spKeyword_";
        }

        public SqlKeywordDao(string tableName, string entityIDName, string storeProcedurePrefix) : base(tableName, entityIDName, storeProcedurePrefix) { }

        public List<Keyword> GetWithFilter(long pagesize, 
            long pagenum, 
            long accountManagerId,
            string keyword, 
            int keywordStatus = 0
            )
        {
            try
            {
                string sql = StoreProcedurePrefix + "GetFilter";
                object[] parms =
                    {
                        "@pagesize", pagesize, 
                        "@pagenum", pagenum, 
                        "@accountManagerId" ,accountManagerId,
                        "@keyword", keyword, 
                        "@keywordStatus", keywordStatus
                    };
                return DbAdapter1.ReadList(sql, Make, true, parms);
            }
            catch (Exception)
            {
                return null;
            }
        }

        public List<Keyword> GetAllKeyword()
        {
            try
            {
                string sql = "select * from tblKeyword where KeywordStatus = 1 ";
                object[] parms = { };
                return DbAdapter1.ReadList(sql, Make, false, parms);
            }
            catch(Exception ex)
            {
                return null;
            }
        }
        

        public int GetTotalPage(long pagesize, 
            long topUpId, 
            long moId, 
            string phoneNumber = "", 
            string message = "", 
            int topUpStatus = 0, 
            int isSentSms = 0, 
            string cardCode = "",
            int fromdate = 0,
            int todate = 0)
        {
            try
            {
                string sql = StoreProcedurePrefix + "GetTotalPage";
                object[] parms =
                    {
                        "@pagesize", pagesize, 
                        "@topupid", topUpId, 
                        "@moid", moId, 
                        "@phonenumber", phoneNumber,
                        "@message", message, 
                        "@topupstatus", topUpStatus, 
                        "@issentsms", isSentSms,
                        "@fromdate", fromdate,
                        "@todate", todate
                    };
                return DbAdapter1.GetCount(sql, true, parms);
            }
            catch (Exception)
            {
                return -1;
            }

        }

        public List<Keyword> SearchTopUp(long topUpId,
            long moId, 
            string phoneNumber = "", 
            string message = "", 
            int topUpStatus = 0, 
            int isSentSms = 0, 
            string cardCode = "", 
            DateTime? fromdate = null,
            DateTime? todate = null)
        {
            try
            {
                string sql = StoreProcedurePrefix + "GetList";
                object[] parms =
                    {
                        "@topupid", topUpId, 
                        "@moid", moId, 
                        "@phonenumber", phoneNumber, 
                        "@message", message,
                        "@topupstatus", topUpStatus, 
                        "@issentsms", isSentSms,
                        "@fromdate", fromdate?? DateTime.MinValue.AddYears(1800),
                        "@todate", (todate!=null && todate!= DateTime.MaxValue)? todate.Value.AddSeconds(86399):DateTime.MaxValue
                    };
                return DbAdapter1.ReadList(sql, Make, true, parms);
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// Lấy thông tin topup thông qua guid topup
        /// </summary>
        /// <param name="guid"></param>
        /// <returns></returns>
        public Keyword GetTopUpWithGuid(string guid)
        {
            try
            {
                string sql = String.Format("{0}GetTopUpWithGuid", StoreProcedurePrefix);
                object[] parms = { "@guid", guid };

                return DbAdapter1.Read(sql, Make, true, parms);
            }
            catch
            {
                return null;
            }
        }

        public List<Keyword> GetDistinctPhone(long pagesize,
            long pagenum,
            long topUpId,
            long moId,
            string phoneNumber = "",
            string message = "",
            int topUpStatus = 0,
            int isSentSms = 0,
            string cardCode = "",
            int fromdate = 0,
            int todate = 0)
        {
            try
            {
                string sql = StoreProcedurePrefix + "GetDistinctPhone";
                object[] parms =
                    {
                        "@pagesize", pagesize, 
                        "@pagenum", pagenum, 
                        "@topupid", topUpId, 
                        "@moid", moId,
                        "@phonenumber", phoneNumber, 
                        "@message", message, 
                        "@topupstatus", topUpStatus,
                        "@issentsms", isSentSms,
                        "@fromdate", fromdate,
                        "@todate", todate
                    };
                return DbAdapter1.ReadList(sql, Make, true, parms);
            }
            catch (Exception)
            {
                return null;
            }
        }

        public int CheckExists(string keyword,string loginName)
        {
            try
            {
                string sql = "spKeyword_CheckExists";
                object[] parms = { "@keyword", keyword, "@loginName", loginName };
                return Convert.ToInt32(DbAdapter1.ExcecuteScalar(sql, true, parms));
            }
            catch (Exception)
            {
                return 0;
            }
        }

        public int ChangeStatus(long keywordId,int keywordStatus)
        {
            try
            {
                string sql = "spKeyword_ChangeStatus";
                object[] parms = { "@keywordId", keywordId, "@keywordStatus", keywordStatus };
                return Convert.ToInt32(DbAdapter1.ExcecuteScalar(sql, true, parms));
            }
            catch (Exception)
            {
                return 0;
            }
        }
    }
}
