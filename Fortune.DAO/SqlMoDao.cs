using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Vina.DTO;

namespace Vina.DAO
{
    public class SqlMoDao : SqlDaoBase<Mo>
    {
        public SqlMoDao()
        {
            TableName = "tblMo";
            EntityIDName = "MoID";
            StoreProcedurePrefix = "spMo_";
        }
        public SqlMoDao(string tableName, string entityIDName, string storeProcedurePrefix) : base(tableName, entityIDName, storeProcedurePrefix) { }
        public List<Mo> GetWithFilter(long pagesize, 
            long pagenum,
            string pinCode,
            DateTime fromdate, 
            DateTime todate,
            string phoneNumber = "", 
            string message = "", 
            int moStatus = 1,
            long moId = -1,
            long accountManagerId = -1
        )
        {
            try
            {
                string sql = StoreProcedurePrefix + "GetFilter";
                object[] parms =
                    {
                        "@pagesize", pagesize, 
                        "@pagenum", pagenum, 
                        "@pinCode", pinCode, 
                        "@phonenumber", phoneNumber,
                        "@message", message, 
                        "@mostatus", moStatus, 
                        "@fromdate", fromdate,
                        "@todate", todate,
                        "@moId", moId,
                        "@accountManagerId",accountManagerId
                    };
                return DbAdapter1.ReadList(sql, Make, true, parms);
            }
            catch (Exception)
            {
                return null;
            }
        }

        public List<Mo> ViewAllMoErrorSyntax(long pagesize, 
            long pagenum,
            string pinCode, 
            DateTime fromdate ,
            DateTime todate,
            string phoneNumber = "", 
            string message = "", 
            int moStatus = 1,
            long moId = -1,
            long accountManagerId = -1
        )
        {
            try
            {
                string sql = "spMo_ViewAllMoErrorSyntax";
                object[] parms =
                    {
                        "@pagesize", pagesize, 
                        "@pagenum", pagenum, 
                        "@pinCode", pinCode, 
                        "@phonenumber", phoneNumber,
                        "@message", message, 
                        "@mostatus", moStatus, 
                        "@fromdate", fromdate,
                        "@todate", todate,
                        "@moId", moId,
                        "@accountManagerId",accountManagerId
                    };
                return DbAdapter1.ReadList(sql, Make, true, parms);
            }
            catch (Exception)
            {
                return null;
            }
        }


        public int CountMoByAccountID(long accountId)
        {
            try
            {
                string sql = "spMo_CountMoByAccountID";
                object[] parms =
                {
                    "@accountId", accountId
                };
                return Convert.ToInt32(DbAdapter1.ExcecuteScalar(sql, true, parms));
            }
            catch (Exception)
            {
                return -1;
            }

        }

        public int GetTotalPage(long pagesize, string CardCode,
            int fromdate,
            int todate,
            string phoneNumber = "",
            string message = "",
            int moType = 0
             )
        {
            try
            {
                string sql = StoreProcedurePrefix + "GetTotalPage";
                object[] parms =
                {
                    "@pagesize", pagesize,
                    "@cardcode", CardCode,
                    "@phonenumber", phoneNumber,
                    "@message", message,
                    "@motype", moType,
                    "@fromdate", fromdate,
                    "@todate", todate
                };
                return DbAdapter1.GetCount(sql, true, parms);
            }
            catch (Exception)
            {
                return -1;
            }

        }

        public IList<Mo> SearchMo(long CardId, string PhoneNumber = "", string Message = "", int MoType = 0)
        {
            try
            {
                string sql = StoreProcedurePrefix + "GetList";
                object[] parms = { "@cardid", CardId, "@phonenumber", PhoneNumber, "@message", Message, "@motype", MoType };
                return DbAdapter1.ReadList(sql, Make, true, parms);
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// Lấy thông tin MO thông qua guid
        /// </summary>
        /// <param name="guid"></param>
        /// <returns></returns>
        public Mo GetMoWithGuid(string guid)
        {
            try
            {
                string sql = String.Format("{0}GetMoWithGuid", StoreProcedurePrefix);
                object[] parms = { "@guid", guid };

                return DbAdapter1.Read(sql, Make, true, parms);
            }
            catch
            {
                return null;
            }
        
        }

        public List<Mo> GetDistinctPhone(long pagesize,
            long pagenum,
            string cardCode,
            int fromdate,
            int todate,
            string phoneNumber = "",
            string message = "",
            int moType = 0
             )
        {
            try
            {
                string sql = StoreProcedurePrefix + "GetDistinctPhone";
                object[] parms =
                    {
                        "@pagesize", pagesize, 
                        "@pagenum", pagenum, 
                        "@cardcode", cardCode, 
                        "@phonenumber", phoneNumber,
                        "@message", message, 
                        "@motype", moType, 
                        "@fromdate", fromdate,
                        "@todate", todate
                    };
                return DbAdapter1.ReadList(sql, Make, true, parms);
            }
            catch (Exception)
            {
                return null;
            }
        }

        public List<Mo> Report(int fromDate,int toDate)
        {
            try
            {
                string sql = "spMo_Report";
                object[] parms = { "@fromDate", fromDate, "@toDate", toDate };
                return DbAdapter1.ReadList(sql, Make, true, parms);
            }
            catch (Exception)
            {
                return null;
            }
        }

    }
}
