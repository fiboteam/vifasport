﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Utility
{
    
    public enum TopUpStatus : short
    {
        /// <summary>
        /// Topup mới khởi tạo
        /// </summary>
        JustCreated = 1,
        /// <summary>
        /// Giao dịch đã được gửi sang Fibo
        /// </summary>
        Pending = 2,
        /// <summary>
        /// Giao dịch đã thành công
        /// </summary>
        Success = 3,
        /// <summary>
        /// Giao dịch thất bại
        /// </summary>
        Fail = 4
    }

    public enum IsSentSms : short
    {
        /// <summary>
        /// Tin vietel có mã card
        /// </summary>
        SentSuccess = 1,
        /// <summary>
        /// Tin send fail
        /// </summary>
        SentFail = 2
    }

    public enum ExcelSheetAt : short
    {
        Sheet1 = 0,
        Sheet2 = 1,
        Sheet3 = 2,
        Sheet4 = 3
    }

    public enum ExcelColunmAt : short
    {
        A = 0,
        B = 1,
        C = 2,
        D = 3,
        E = 4
    }

    public enum MoStatus : short
    {
        /// <summary>
        /// Topup mới khởi tạo
        /// </summary>
        JustCreated = 1,
        /// <summary>
        /// Giao dịch đã được gửi sang Fibo
        /// </summary>
        WrongPinCode = 2,
        
        /// <summary>
        /// Giao dịch đã thành công
        /// </summary>
        Success = 3,
        /// <summary>
        /// Giao dịch thất bại
        /// </summary>
        //
        WrongSyntax = 4,
        PinCodeIsUsed = 5,
        AccountIsDeactive = 6,
		WrongPhoneNumber=7,
		Exception=8,
		WrongSyntaxMain=9,
		WrongSyntaxCustomer=10,
		WrongCustomer=11,
		SuccessWithCustomer=12,
    }

	public enum MTStatus:short
	{
		Sending=1,
		Success=2,
		Fail=3,
		Error=4
	}

    public enum SubKeyStatus : short
    {
        /// <summary>
        /// Topup mới khởi tạo
        /// </summary>
        Active = 1,
        /// <summary>
        /// Giao dịch đã được gửi sang Fibo
        /// </summary>
        DeActive = 2,
        /// <summary>
        /// Giao dịch đã thành công
        /// </summary>
        Deleted = 3
    }

    public enum KeywordStatus : short
    {
        /// <summary>
        /// Topup mới khởi tạo
        /// </summary>
        Active = 1,
        /// <summary>
        /// Giao dịch đã được gửi sang Fibo
        /// </summary>
        DeActive = 2,
        /// <summary>
        /// Giao dịch đã thành công
        /// </summary>
        Deleted = 3
    }

    public enum PinCodeStatus : short
    {
        /// <summary>
        /// Topup mới khởi tạo
        /// </summary>
        JustCreated = 1,
        /// <summary>
        /// Giao dịch đã được gửi sang Fibo
        /// </summary>
        Used = 2,
        /// <summary>
        /// Giao dịch đã thành công
        /// </summary>
        Deleted = 3
    }

    public enum MtType : short
    {
		/// <summary>
		/// trả về khi nhắn tin lần đầu và không có ng gioi thiều
		/// </summary>
        Success = 1, 
		/// <summary>
		/// Sai syntax
		/// </summary>
        WrongSystax = 2,
		/// <summary>
		/// Sai pin code
		/// </summary>
        WrongPinCode = 3, 
		/// <summary>
		/// Pincode đã sử dụng
		/// </summary>
        PinCodeIsUsed = 4,
        SubKeyIsDeactive = 5,
		/// <summary>
		/// SDT này đã có mã cutomer, vui lòng nt theo cú pháp ng giới thiệu
		/// </summary>
		WrongSyntax2 = 6, //
		/// <summary>
		/// trả về cho ng gioi thiệu
		/// </summary>
		Success2 = 7,
		/// <summary>
		/// Trả về tin chiết khấu
		/// </summary>
		Success3 =8,
		WrongCustomer=9,
    }

    public enum PinCodeBlockStatus : short
    {
        Active = 1,
        DeActive = 2,
        Deleted = 3
    }

    public enum PinCodeListType : short
    {
        Success = 1,
        Duplicated = 2,
        WrongSyntax = 3
    }

    public enum ProductTypeStatus : short
    {
        Active = 1,
        DeActive = 2,
        Deleted = 100
    }
}
