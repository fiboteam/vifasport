﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VinaVN.Models
{
    public class ConfigModel
    {
        public ConfigModel()
        {
            StartDate = DateTime.Now;
            EndDate = DateTime.Now.AddDays(30);
        }
        /// <summary>
        /// Vị trí trang hiện tại
        /// </summary>
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

    }
}