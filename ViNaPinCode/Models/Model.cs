﻿using VinaVN.Code;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VinaVN.Models
{
    public class Model
    {
        public long ID { get; set; }
        public string ErrorMessage { get; set; }
        public string SuccessMessage { get; set; }
        public Paging Paging { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        

    }
}