﻿using Vina.DTO;
using VinaVN.Code;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Utility;

namespace VinaVN.Models
{
    public class PinCodeViewModel : Model
    {
        public PinCodeViewModel()
        {
            AccountManagerID = -1;
            Page = new Paging() { PageSize = 100 };
            PinCode = "";
            Serial = "";
            Serial2 = "";
            DataList = new List<PinCodeObj>();
            PinCodeStatus = 0;
            FromDate = DateTime.Now.AddDays(-365);
            ToDate = DateTime.Now.AddDays(365);
            PinCodeBlockID = 0;
        }
        /// <summary>
        /// Vị trí trang hiện tại
        /// </summary>
        public int CurrentPage { get; set; }
        /// <summary>
        /// MoId của MT
        /// </summary>
        public long AccountManagerID { get; set; }
        public long ProductTypeID { get; set; }
        /// <summary>
        /// Control phân trang
        /// </summary>
        /// 
        public PinCodeStatus PinCodeStatus { get; set; }

        public long PinCodeBlockID { get; set; }

        public Paging Page { get; set; }

        /// <summary>
        /// Số phone
        /// </summary>
        public string PinCode { get; set; }

        public string Serial { get; set; }
        public string Serial2 { get; set; }
        /// <summary>
        /// Dữ liệu hiển thị lên viewmodel
        /// </summary>
        public List<PinCodeObj> DataList { get; set; }

        /// <summary>
        /// Truyền thông tin qua URL IsPopup= true để show thêm khung search hoặc chỉ show Grid dữ liệu
        /// </summary>
        public bool IsPopup { get; set; }

        public string Captcha { get; set; }
     
    }
}