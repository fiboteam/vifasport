﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VinaVN.Models
{
    public class CheckingViewModel
    {
        public string PinCode { get; set; }
        public string Captcha { get; set; }
    }
}