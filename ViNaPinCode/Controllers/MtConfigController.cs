﻿using Vina.DAO;
using Vina.DTO;
using VinaVN.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace VinaVN.Controllers
{
    public class MtConfigController : BaseController
    {
        //
        // GET: /MtConfig/
        private readonly SqlMtConfigDao _mtConfigDao = new SqlMtConfigDao();
		#region no
		//public ActionResult Index()
		//{
		//	List<MtConfig> listConfig = _mtConfigDao.GetConfigByAccountID(SAccount.ID);
		//	MtConfigViewModel model = new MtConfigViewModel();
		//	try
		//	{
		//		model.MtForSuccess = listConfig.Where(m => m.MtType == Utility.MtType.Success).FirstOrDefault().MtMessage;
		//		model.MtForWrongSystax = listConfig.Where(m => m.MtType == Utility.MtType.WrongSystax).FirstOrDefault().MtMessage;
		//		model.MtForWrongPinCode = listConfig.Where(m => m.MtType == Utility.MtType.WrongPinCode).FirstOrDefault().MtMessage;
		//		model.MtForPinCodeIsUsed = listConfig.Where(m => m.MtType == Utility.MtType.PinCodeIsUsed).FirstOrDefault().MtMessage;
		//		model.MtForSubKeyIsDeactive = listConfig.Where(m => m.MtType == Utility.MtType.SubKeyIsDeactive).FirstOrDefault().MtMessage;
		//		model.MtForWrongSyntax2 = listConfig.Where(m => m.MtType == Utility.MtType.WrongSyntax2).FirstOrDefault().MtMessage;
		//		model.MtForSuccess2 = listConfig.Where(m => m.MtType == Utility.MtType.Success2).FirstOrDefault().MtMessage;
		//		model.MtForSuccess3 = listConfig.Where(m => m.MtType == Utility.MtType.Success3).FirstOrDefault().MtMessage;
		//		model.MtForWrongCustomer = listConfig.Where(m => m.MtType == Utility.MtType.WrongCustomer).FirstOrDefault().MtMessage;
		//	}
		//	catch(Exception ex)
		//	{
		//		model.MtForSuccess = "";
		//		model.MtForWrongSystax = "";
		//		model.MtForWrongPinCode = "";
		//		model.MtForPinCodeIsUsed = "";
		//		model.MtForSubKeyIsDeactive = "";
		//		model.MtForWrongSyntax2 = "";
		//		model.MtForSuccess2 = "";
		//		model.MtForSuccess3 = "";
		//		model.MtForWrongCustomer = "";
		//	}
		//	ViewBag.SuccessMessage = (Session["SuccessMessage"] != null && Session["SuccessMessage"] != "") ? Session["SuccessMessage"] : "";
		//	ViewBag.ErrorMessage = (Session["ErrorMessage"] != null && Session["ErrorMessage"] != "") ? Session["ErrorMessage"] : "";
		//	Session["SuccessMessage"] = "";
		//	Session["ErrorMessage"] = "";
		//	return View(model);
		//}

		//[HttpPost]
		//[ValidateInput(false)]
		//public ActionResult SaveConfigChange(MtConfigViewModel model)
		//{
		//	//if (model.MtForSuccess.Length > 160 || model.MtForWrongSystax.Length > 160 || model.MtForWrongPinCode.Length > 160 || model.MtForPinCodeIsUsed.Length > 160 || model.MtForSubKeyIsDeactive.Length > 160)
		//	//{
		//	//	Session["ErrorMessage"] = "MT không được cập nhật do có MT vượt quá 160 ký tự";
		//	//	return Redirect("/MtConfig");
		//	//}
		//	try
		//	{
		//		MtConfig mtConfigSuccess = new MtConfig();
		//		MtConfig mtConfigWrongSystax = new MtConfig();
		//		MtConfig mtConfigWrongPinCode = new MtConfig();
		//		MtConfig mtConfigPinCodeIsUsed = new MtConfig();
		//		MtConfig mtConfigTimeOutOfThisCampaign = new MtConfig();
		//		MtConfig mtConfigWrongSyntax2 = new MtConfig();
		//		MtConfig mtConfigSuccess2 = new MtConfig();
		//		MtConfig mtConfigSuccess3 = new MtConfig();
		//		MtConfig mtConfigWrongCustomer = new MtConfig();

		//		mtConfigSuccess.MtMessage = model.MtForSuccess;
		//		mtConfigSuccess.AccountManagerID = SAccount.ID;
		//		mtConfigSuccess.MtType = Utility.MtType.Success;
		//		_mtConfigDao.Insert(mtConfigSuccess);

		//		mtConfigWrongSystax.MtMessage = model.MtForWrongSystax;
		//		mtConfigWrongSystax.AccountManagerID = SAccount.ID;
		//		mtConfigWrongSystax.MtType = Utility.MtType.WrongSystax;
		//		_mtConfigDao.Insert(mtConfigWrongSystax);

		//		mtConfigWrongPinCode.MtMessage = model.MtForWrongPinCode;
		//		mtConfigWrongPinCode.AccountManagerID = SAccount.ID;
		//		mtConfigWrongPinCode.MtType = Utility.MtType.WrongPinCode;
		//		_mtConfigDao.Insert(mtConfigWrongPinCode);

		//		mtConfigPinCodeIsUsed.MtMessage = model.MtForPinCodeIsUsed;
		//		mtConfigPinCodeIsUsed.AccountManagerID = SAccount.ID;
		//		mtConfigPinCodeIsUsed.MtType = Utility.MtType.PinCodeIsUsed;
		//		_mtConfigDao.Insert(mtConfigPinCodeIsUsed);

		//		mtConfigTimeOutOfThisCampaign.MtMessage = model.MtForSubKeyIsDeactive;
		//		mtConfigTimeOutOfThisCampaign.AccountManagerID = SAccount.ID;
		//		mtConfigTimeOutOfThisCampaign.MtType = Utility.MtType.SubKeyIsDeactive;
		//		_mtConfigDao.Insert(mtConfigTimeOutOfThisCampaign);

		//		mtConfigWrongSyntax2.MtMessage = model.MtForWrongSyntax2;
		//		mtConfigWrongSyntax2.AccountManagerID = SAccount.ID;
		//		mtConfigWrongSyntax2.MtType = Utility.MtType.WrongSyntax2;
		//		_mtConfigDao.Insert(mtConfigWrongSyntax2);

		//		mtConfigSuccess2.MtMessage = model.MtForSuccess2;
		//		mtConfigSuccess2.AccountManagerID = SAccount.ID;
		//		mtConfigSuccess2.MtType = Utility.MtType.Success2;
		//		_mtConfigDao.Insert(mtConfigSuccess2);

		//		mtConfigSuccess3.MtMessage = model.MtForSuccess3;
		//		mtConfigSuccess3.AccountManagerID = SAccount.ID;
		//		mtConfigSuccess3.MtType = Utility.MtType.Success3;
		//		_mtConfigDao.Insert(mtConfigSuccess3);

		//		mtConfigWrongCustomer.MtMessage = model.MtForWrongCustomer;
		//		mtConfigWrongCustomer.AccountManagerID = SAccount.ID;
		//		mtConfigWrongCustomer.MtType = Utility.MtType.WrongCustomer;
		//		_mtConfigDao.Insert(mtConfigWrongCustomer);

		//		Session["SuccessMessage"] = "Nội dung MT đã được cập nhật thành công";
		//	}
		//	catch(Exception ex)
		//	{
		//		Session["ErrorMessage"] = "MT không được cập nhật do có lỗi trong quá trình khôi phục dữ liệu";
		//	}
           
		//	return Redirect("/MtConfig");
		//}

		//public ActionResult ConfigDefaultWrongSyntaxMessage()
		//{
		//	DefaultWrongSyntaxMessageViewModel model = new DefaultWrongSyntaxMessageViewModel();
		//	MtConfig conf = _mtConfigDao.GetDefaultWrongSyntaxMessage();
		//	if(conf != null)
		//	{
		//		model.DefaultWrongSyntaxMessage = conf.MtMessage;
		//	}
		//	return View(model);
		//}

		//[HttpPost]
		//[ValidateInput(false)]
		//public ActionResult ConfigDefaultWrongSyntaxMessage(DefaultWrongSyntaxMessageViewModel model)
		//{
		//	if (model.DefaultWrongSyntaxMessage.Length > 160)
		//	{
		//		ViewBag.ErrorMessage = "MT không được cập nhật do có MT vượt quá 160 ký tự";
		//		return Redirect("/MtConfig");
		//	}

		//	try
		//	{
		//		if (_mtConfigDao.UpdateDefaultWrongSyntaxMessage(model.DefaultWrongSyntaxMessage) > 0)
		//		{
		//			ViewBag.SuccessMessage = "Đã cập nhật thành công";
		//		}
		//		else
		//		{
		//			ViewBag.ErrorMessage = "Cập nhật thất bại";
		//		}
		//	}
		//	catch
		//	{
		//		ViewBag.ErrorMessage = "Cập nhật thất bại";
		//	}
		//	return View(model);
		//}
		#endregion no
    }
}
