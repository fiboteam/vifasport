﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utility;

namespace Vina.DTO
{
    public class PinCodeBlock : BusinessObject
    {
        public PinCodeBlock()
        {
            PinCodeBlockName = "";
            PinCodeBlockStatus = 0;
            AccountManagerID = -1;
        }
        public string PinCodeBlockName { get; set; }
        public PinCodeBlockStatus PinCodeBlockStatus { get; set; }
        public long AccountManagerID { get; set; }
    }
}
