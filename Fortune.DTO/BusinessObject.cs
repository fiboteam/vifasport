﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;
using System.Reflection;
using System.ComponentModel;
namespace Vina.DTO
{
    public abstract class BusinessObject
    {
        protected long id = -1;

        public virtual long ID
        {
            get { return id; }
            set
            {
                id = value;
            }
        }

        public string EcryptedID { get; set; }
        private IDictionary<string, object> dynamicProperties = new Dictionary<string, object>();

        public IDictionary<string, object> ExtentionProperty
        {
            get
            {
                return dynamicProperties;
            }
            set
            {
                dynamicProperties = value;
            }
        }

        public DateTime? CreatedDate { get; set; }

        public DateTime? UpdatedDate { get; set; }

        public BusinessObject()
        {
            CreatedDate = DateTime.Now;
            UpdatedDate = DateTime.Now;
        }
    }


}
