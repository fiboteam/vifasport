using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;
using Utility;

namespace Vina.DTO
{
    public class Keyword : BusinessObject
    {
        public Keyword()
            : base()
        {
            AccountManagerID = -1;
            KeywordValue = "";
            KeywordStatus = KeywordStatus.Active;
        }

        public long AccountManagerID { get; set; }

        public string KeywordValue { get; set; }
        public int TotalProductType { get; set; }

        public KeywordStatus KeywordStatus { get; set; }
    }
}
