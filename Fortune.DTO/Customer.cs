using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Text;

namespace Vina.DTO
{
   
    public class Customer : BusinessObject
    {

		public Customer():base(){
		}

		
		public long ReferralCustomerId { get; set; } 

		
		public string CustomerCode { get; set; } 

		
		public string CustomerName { get; set; } 

		
		public string CustomerPhone { get; set; } 

		
		public string CustomerAddress { get; set; } 

		
		public string CustomerEmail { get; set; } 

		
		public string CustomerNote { get; set; } 

		
		public string CustomerRemark { get; set; } 


    }
}